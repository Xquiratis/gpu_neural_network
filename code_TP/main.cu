// Compile gcc -o ./ann main.c matrix.c ann.c mnist.c -lm

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "mnist.h"
#include "matrix.h"
#include "ann.h"
#include <math.h>
#include <string.h>
#include <time.h>

void populate_minibatch(double *x, double* y, unsigned* minibatch_idx, unsigned minibatch_size, image * img, unsigned img_size, byte* label, unsigned label_size);

void zero_to_n(unsigned n, unsigned* t)
{
    for (unsigned i = 0; i < n; i++)
    {
        t[i] = i;
    }
}

void shuffle(unsigned *t, const unsigned size, const unsigned number_of_switch)
{
    zero_to_n(size, t);
    for (unsigned i = 0; i < number_of_switch; i++)
    {
        unsigned x = rand() % size;
        unsigned y = rand() % size;
        unsigned tmp = t[x];
        t[x] = t[y];
        t[y] = tmp;
    }
}

double sigmoid(double x)
{
    return 1 / (1 + exp(-x));
}

double dsigmoid(double x)
{
    return sigmoid(x)*(1-sigmoid(x));
}

double accuracy(image* test_img, byte* test_label, unsigned datasize, unsigned minibatch_size, ann_t *nn)
{
    unsigned good = 0;
    //unsigned idx[10000];    
    unsigned *idx = (unsigned *) malloc(datasize * sizeof(unsigned)); // Allocation dynamique pour idx
    double *x = (double *) malloc( 28 * 28 * minibatch_size * sizeof(double));
    double *y = (double *) malloc( 10 * minibatch_size * sizeof(double) );

    zero_to_n(datasize, idx);
    
    for (int i = 0; i < datasize - minibatch_size; i+= minibatch_size)
    {        
        populate_minibatch(x, y, &idx[i], minibatch_size, test_img, 28*28, test_label, 10);
        memcpy(nn->layers[0]->activations->m, x, 28*28 * minibatch_size * sizeof(double));     
        forward(nn, sigmoid);
        for (int col = 0; col < minibatch_size; col ++)
        {
            int idxTrainingData = col + i ;
            double max = 0;
            unsigned idx_max = 0;
            for (int row = 0; row < 10; row++){
                int idx = col + row * minibatch_size;
                if (nn->layers[nn->number_of_layers-1]->activations->m[idx] > max){
                    max = nn->layers[nn->number_of_layers-1]->activations->m[idx];
                    idx_max = row;
                }
            }
            if (idx_max == test_label[idxTrainingData])
            {
                good ++;
            }
        }
    }    
    free(idx);
    free(x);
    free(y);

    unsigned ntests = (datasize/minibatch_size) * minibatch_size;
    return (100.0* (double) (good) / ntests );
}

void populate_minibatch(double * x, double * y, unsigned * minibatch_idx, unsigned minibatch_size, image * img, unsigned img_size, byte* label, unsigned label_size)
{
    for (int col = 0; col < minibatch_size; col ++)
    {
        for (int row = 0; row < img_size; row ++)
        {
            x[row * minibatch_size + col] = (double) img[minibatch_idx[col]][row]/255.;
        }

        for (int row = 0; row < 10; row ++)
        {
            y[row * minibatch_size + col] = 0.0;
        }

        y[ label[minibatch_idx[col]] * minibatch_size + col] = 1.0;
    }
}

int main(int argc, char *argv[])
{
     
    // Initialisation de la graine pour la génération de nombres aléatoires
    srand(time(0));

    // Déclaration des variables pour la taille des données
    unsigned datasize, ntest;

    // Lecture des images et des labels d'entraînement
    image* train_img = read_images("../mnist/train-images-idx3-ubyte", &datasize);
    byte* train_label = read_labels("../mnist/train-labels-idx1-ubyte", &datasize);

    // Lecture des images et des labels de test
    image* test_img = read_images("../mnist/t10k-images-idx3-ubyte", &ntest);
    byte* test_label = read_labels("../mnist/t10k-labels-idx1-ubyte", &ntest);

      

    // Déclaration et initialisation du réseau de neurones artificiels
    ann_t * nn;
    double alpha = 0.05; // Taux d'apprentissage
    unsigned minibatch_size = 16; // Taille des mini-lots
    unsigned number_of_layers = 3; // Nombre de couches
    unsigned nneurons_per_layer[3] = {28*28, 30, 10}; // Neurones par couche

    // Création du réseau de neurones
    
    nn = create_ann(alpha, minibatch_size, number_of_layers, nneurons_per_layer);
    

    // Affichage de la précision initiale
    printf("starting accuracy %lf\n", accuracy(test_img, test_label, ntest, minibatch_size, nn));

    // Allocation de mémoire pour les indices mélangés, les entrées et les sorties des mini-lots
    unsigned *shuffled_idx = (unsigned *)malloc(datasize * sizeof(unsigned));
    double *x = (double *) malloc(28 * 28 * minibatch_size * sizeof(double));
    double *y = (double *) malloc(10 * minibatch_size * sizeof(double));
    matrix_t *out = alloc_matrix(10, minibatch_size);

    // Boucle d'apprentissage sur 40 époques
    for (int epoch = 0; epoch < 40; epoch++)
    {
        //etape 1 : initialisation des mesures
        float mesure3 = 0.0;
        float mesure4 = 0.0;
        float mesure5 = 0.0;
        float mesure6 = 0.0;
        float mesure7 = 0.0;

        clock_t start3 = 0.0;
        clock_t start4 = 0.0;
        clock_t start5 = 0.0;
        clock_t start6 = 0.0;
        clock_t start7 = 0.0;
        clock_t end3 = 0.0;
        clock_t end4 = 0.0;
        clock_t end5 = 0.0;
        clock_t end6 = 0.0;
        clock_t end7 = 0.0;


        printf("start learning epoch %d\n", epoch);

        // Mélange des indices des données d'entraînement
        shuffle(shuffled_idx, datasize, datasize);

        // Boucle sur les mini-lots
        for (int i = 0; i < datasize - minibatch_size; i += minibatch_size)
        {
        
            //etape 1 : commencer mesure temporelle
            start3 = clock();
            // Remplissage du mini-lot avec les données mélangées
            populate_minibatch(x, y, shuffled_idx + i, minibatch_size, train_img, 28 * 28, train_label, 10);
            //etape 1 : terminer et calculer mesure temporelle
            end3 = clock();
            mesure3 += ((float)(end3 - start3) / CLOCKS_PER_SEC);

            //etape 1 : commencer mesure temporelle
            start4 = clock();
            // Copie des entrées du mini-lot dans la première couche du réseau
            memcpy(nn->layers[0]->activations->m, x, 28 * 28 * minibatch_size * sizeof(double));
            //etape 1 : terminer et calculer mesure temporelle
            end4 = clock();
            mesure4 += ((float)(end4 - start4) / CLOCKS_PER_SEC);

            //etape 1 : commencer mesure temporelle
            start5 = clock();
            // Propagation avant à travers le réseau
            forward(nn, sigmoid);
            //etape 1 : terminer et calculer mesure temporelle
            end5 = clock();
            mesure5 += ((float)(end5 - start5) / CLOCKS_PER_SEC);

            //etape 1 : commencer mesure temporelle
            start6 = clock();
            // Copie des sorties attendues dans la matrice de sortie
            memcpy(out->m, y, 10 * minibatch_size * sizeof(double));
            //etape 1 : terminer et calculer mesure temporelle
            end6 = clock();
            mesure6 += ((float)(end6 - start6) / CLOCKS_PER_SEC);
            
            //etape 1 : commencer mesure temporelle
            start7 = clock();
            // Rétropropagation de l'erreur à travers le réseau
            backward(nn, out, dsigmoid);
            //etape 1 : terminer et calculer mesure temporelle
            end7 = clock();
            mesure7 += ((float)(end7 - start7) / CLOCKS_PER_SEC);
        }

        // Affichage de la précision après chaque époque
        printf("epoch %d accuracy %lf\n", epoch, accuracy(test_img, test_label, ntest, minibatch_size, nn));

        //etape 1 : affichage des mesures temporelles
        printf("mesure temporelle remplissage du mini-lot avec les donnees melangees: %f sec\n", mesure3);
        printf("mesure temporelle copie des entrees du mini-lot dans la premiere couche du reseau: %f sec\n", mesure4);
        printf("mesure temporelle propagation avant a travers le reseau: %f sec\n", mesure5);
        printf("mesure temporelle copie des sorties attendues dans la matrice de sortie: %f sec\n", mesure6);
        printf("mesure temporelle retropropagation de l'erreur a travers le reseau: %f sec\n", mesure7);
    }

    free(x);
    free(y);
    free(shuffled_idx);
    destroy_matrix(out);   
    
    return 0;
}