## savoir où l'on est
git status 

## ajout des fichier
git add * 

## local ajout
git commit -m "nom du push"

## Ajout sur le git
git push

## changer de branch
git checkout nom_de_la_branch 

## ------------- ##
## compilation 
gcc -c .\ann.c .\main.c .\matrix.c .\mnist.c 

# génération d'un nom_du_fichier.o
gcc -o out .\ann.o .\main.o .\matrix.o .\mnist.o

## lancement
.\out.exe

## Pour GPU
nvcc -o outnvcc .\ann.cu .\main.cu .\matrix.cu .\mnist.cu

## pour lancer
.\outnvcc.exe